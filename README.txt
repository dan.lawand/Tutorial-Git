# Tutorial-Git: to help me dealing with git


User Manual from git site
https://git-scm.com/docs/user-manual.html


Quick setup — if you’ve done this kind of thing before
or	
HTTPS / SSH " https://github.com/danlawand/Tutorial-Git.git " 
Get started by creating a new file or uploading an existing file. We recommend every repository include a README, LICENSE, and .gitignore.

…or create a new repository on the command line
echo "# Tutorial-Git" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/danlawand/Tutorial-Git.git
git push -u origin master

…or push an existing repository from the command line
git remote add origin https://github.com/danlawand/Tutorial-Git.git
git push -u origin master

…or import code from another repository
You can initialize this repository with code from a Subversion, Mercurial, or TFS project.


You've changed  a file and want to refresh:

git add file.txt
git commit -m "massage"
git push -u origin master
